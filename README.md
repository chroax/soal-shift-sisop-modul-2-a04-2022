# soal-shift-sisop-modul-2-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# Soal 1
## Catatan
- Menggunakan fork dan exec.
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename().
- Tidak boleh pake cron.
- Semua poin dijalankan oleh 1 script di latar belakang. Cukup jalankan script 1x serta ubah time dan date untuk check hasilnya.

## A. Download file dan extract
File didownload menggunakan fungsi wget dengan url yang disediakan. File ditaruh dalam folder bernama gacha_gacha sebagai working directory sesuai perintah pada soal. Untuk mengatasi persyaratan tidak digunakannya mkdir(), maka digunakan pernitah execlp.  Kedua file diberikan nama yang sesuai dan diunzip menggunakan fungsi unzip. Digunakan juga "while(wait(&status)) > 0" agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan.

```
 void downloadExtract()
{
    char *urls[] = {"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download",
                    "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download"};

    char *filename[] = {"characters.zip", "weapons.zip"};
    id_t child_id;
    int status;

    if ((child_id = fork()) == 0)
    {
        execlp("mkdir", "mkdir", "-p", "gacha_gacha", NULL);
    }

    for (int i = 0; i < 2; i++)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("wget", "wget", "--no-check-certificate", urls[i], "-O", filename[i], "-q", NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("unzip", "unzip", "-qq", filename[i], NULL);
        }

        while ((wait(&status)) > 0)
            ;

        if ((child_id = fork()) == 0)
        {
            execlp("rm", "rm", "-f", filename[i], NULL);
        }
        while ((wait(&status)) > 0)
            ;
    }
}
```
## B. Simulasi Gacha
Simulasi gacha dilakukan dengan fungsi gacha yang pertama memastikan bahwa working directory adalah gacha_gacha.

```
void gacha(int gacha_counter)
{
    id_t child_id;
    int status;
    if (gacha_counter == 0)
    {
        if ((chdir("gacha_gacha")) < 0)
        {
            exit(EXIT_FAILURE);
        }
    }

```

Untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut.

```
    if (gacha_counter % 10 == 0)
    {
        char r_dir_name[30] = "";
        strcat(r_dir_name, dir_name);
        if ((chdir(r_dir_name)) < 0)
        {
            exit(EXIT_FAILURE);
        }
        srand(time(NULL));
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = formatTime(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_counter);
            strcat(strcat(file_name, buffer2), ".txt");
```

Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. 
```
if (gacha_counter % 90 == 0)
    {
        if ((child_id = fork()) == 0)
        {
            execlp("mkdir", "mkdir", "-p", dir_name, NULL);
        }

        while ((wait(&status)) > 0)
            ;
    }
```

Untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters.

```
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, get_random_file("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, get_random_file("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
```

## C. Penamaan File Gacha
Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Sebelumnya dibuat struct formatTime untuk melakukan setting waktu sebagai berikut. 

```
struct tm *formatTime(struct tm *inputTime, int addition)
{
    if (inputTime->tm_sec + addition >= 60)
    {
        inputTime->tm_min++;
        inputTime->tm_sec = addition;
        if (inputTime->tm_min >= 60)
        {
            inputTime->tm_hour++;
            inputTime->tm_min = 0;
            if (inputTime->tm_hour >= 24)
            {
                inputTime->tm_hour = 0;
            }
        }
    }
    else
    {
        inputTime->tm_sec += addition;
    }
    return inputTime;
}
```

Baru kemudian dibuat file dengan penamaan sesuai aturan.

```
        for (int i = 1; i < 10; i++)
        {
            char file_name[25], buffer2[10];
            struct tm *formattedTime = formatTime(myTime, 1);
            strftime(file_name, sizeof(file_name), "%H:%M:%S", formattedTime);
            strcat(file_name, "_gacha_");
            snprintf(buffer2, 10, "%d", (i * 10) + gacha_counter);
            strcat(strcat(file_name, buffer2), ".txt");

            if ((child_id = fork()) == 0)
            {
                execlp("touch", "touch", file_name, NULL);
            }
            while ((wait(&status)) > 0)
                ;
```
Untuk penjedaan 1 detik, digunakan fungsi sleep(1) yang akan dipanggil pada akhir fungsi main.


## D. Mekanik Gacha
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity.

Pertama-tama, definisikan variable primo sebagai primo_avail, kemudian buat teknis gacha sesuai yang diberikan pada soal. Kuantitas primo awalnya didefinisikan sebagai 79000 yang kemudian dikurangi 160 tiap kali gacha dilakukan. Output gacha akan ditaruh pada file yang disediakan pada poin A setiap sepuluh iterasi. Output akan ditaruh sesuai dengan tipenya yaitu karakter atau weapon yang ditentukan oleh ganjil atau genapnya gacha_counter. Proses pengacakan dilakukan dengan fungsi rand(). Proses gacha akan berakhir jika primo_avail memiliki nilai yang sudah tidak cukup lagi untuk dilakukan gacha.
```
            int primo_avail;
            FILE *fp;
            fp = fopen(file_name, "a");
            for (int j = 1; j <= 10; j++)
            {
                primo_avail = 79000 - 160 * ((j) + (i * 10) + gacha_counter - 10);
                if (primo_avail < 0)
                    break;
                if (j % 2 != 0)
                {
                    char json_path[70] = "../../characters/";
                    strcat(json_path, get_random_file("../../characters", rand() % (48 - 1) + 1));
                    fprintf(fp, "%d_characters_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
                else
                {
                    char json_path[70] = "../../weapons/";
                    strcat(json_path, get_random_file("../../weapons", rand() % (130 - 1) + 1));
                    fprintf(fp, "%d_weapons_%s_%d\n", (j) + (i * 10) + gacha_counter - 10, file_to_json(json_path), primo_avail);
                }
            }
            fclose(fp);
            if (primo_avail < 0)
            {
                return;
            }
```
## E. Waktu Gacha, Penyimpanan Hasil Gacha, dan Penghapusan Jejak Gacha
Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. Untuk mengatur waktu ini, karena pada persyaratan diminta untuk tidak menggunakan cron, maka kita menggunakan fungsi strftime dan memasukkan secara manual waktu yang diminta pada soal.

```
 while (1)
    {
        id_t child_id;
        int status;

        time_t currentTime;
        time(&currentTime);
        struct tm *currTime = localtime(&currentTime);
        char time_var[15];
        strftime(time_var, sizeof(time_var), "%H:%M:%S_%d-%m", currTime);
        if (strcmp(time_var, "04:44:00_30-03") == 0)
        {
            int primo = 79000, i = 0;
            downloadExtract();
            while (primo > 0)
            {
                gacha(i);
                i += 90;
                primo -= 14400;
            }
        }
```
Tiga jam setelah gacha dilakukan (07:44:00_30-03), semua isi folder gacha_gacha akan dizip dan dinamai not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip) Pengaturan waktu dilakukan secara manual lagi dengan strftime, pengezipan dilakukan dengan execlp zip dengan argumen -P untuk menambahkan password. 

```
        else if (strcmp(time_var, "07:44:00_30-03") == 0)
        {
            if ((chdir("../../")) < 0)
            {
                exit(EXIT_FAILURE);
            }
            char cwd[PATH_MAX];
            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                printf("Current working dir: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
            }
            if ((child_id = fork()) == 0)
            {
                execlp("zip", "zip", "-P", "satuduatiga", "-r", "not_safe_for_wibu", "-q", "gacha_gacha", NULL);
            }
```
Setelah itu, kita bisa menghapus sisa file yang ada di folder gacha_gacha secara satu per satu dengan fungsi -rm agar folder zip yang kita buat tidak ikut terhapus. 

```
            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "gacha_gacha", NULL);
            }

            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "characters", NULL);
            }
            while ((wait(&status)) > 0)
                ;

            if ((child_id = fork()) == 0)
            {
                execlp("rm", "rm", "-r", "weapons", NULL);
            }

            while ((wait(&status)) > 0)
                ;
```

## Kendala
- Mencari fungsi untuk mengeksekusi program pada waktu tertentu tanpa menggunakan cron.
- Command yang berasal dari library json tidak terregister.
- Download file di WSL mengembalikan 403 forbidden.

##Output

#### 1. Kompilasi file soal1.c
![satu](img/1.1.png)

#### 2. Menjalankan program file soal1
![dua](img/1.2.png)

#### 3. Hasil download yang sudah diekstrak
![tiga](img/1.3.png)

#### 4. Pembuatan folder dan file gacha pada 4:44
![empat](img/1.4.png)

#### 5. Pengezipan file pada 7:44
![lima](img/1.5.png)

#### 6. Isi file zip
![enam](img/1.6.png)

# Soal 2
## Catatan
- File zip berada dalam drive modul shift ini bernama drakor.zip
- File yang penting hanyalah berbentuk .png
- Setiap foto poster disimpan sebagai nama foto dengan format [nama]:[tahun rilis]:[kategori]. Jika terdapat lebih  dari satu drama dalam poster, dipisahkan menggunakan underscore(_).
- Tidak boleh menggunakan fungsi system(), mkdir(), dan rename() yang tersedia di bahasa C.
- Gunakan bahasa pemrograman C (Tidak boleh yang lain).
- Folder shift2, drakor, dan kategori dibuatkan oleh program (Tidak Manual).
- [user] menyesuaikan nama user linux di os anda.

## A. Membuat Folder dan Mengekstrak File
### 1. Membuat Folder shift2 dan drakor di dalamnya
Pada tahap ini digunakan execlp untuk membuat folder yang dibutuhkan dengan tag "-p" agar apabila parent folder belum ada maka akan dibuat sekaligus dalam sekali pemanggilan. Penggunaan while((wait(&status)) untuk menghindari exit function sebelum script selesai dijalankan. Sintaksnya terletak pada fungsi dengan nama makeFolder dengan parameter path folder yang akan dibuat.
```
void makeFolder(char path[]){
    if (fork() == 0){
        execlp("mkdir", "mkdir", "-p", path, NULL);
    }

    while ((wait(&status)) > 0);
}
```

### 2. Mengekstrak File drakor.zip
Pada tahap ini juga digunakan execv untuk menjalankan command unzip dengan tag “-q” agar tidak menghasilkan verbose pada terminal, kemudian tag “-d” untuk menunjuk directory tujuan dan regex “.png” agar file yang terekstrak hanya yang berformat png saja.
```
void unzip(char destination[], char target[]){
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0){

        char *argv[] = {"unzip", "-q", target, "*.png", "-d", destination, NULL};
        execv("/bin/unzip", argv);
    }

    while ((wait(&status)) > 0);
}
```

## B. Mengkategorikan Poster Film Sesuai Genre
Untuk melaksanakan tugas ini, program akan memanggil fungsi dengan nama classify. Pada fungsi ini akan dilakukan pemerosesan file pada directory drakor. Pertama-tama program akan membuka folder drakor dan melakukan pengecekan nama file (dir->d_name) selama tidak NULL. Pada setiap pengulangan akan dilakukan:
### 1. Menghapus Format “.png” File
Proses menggunakan bantuan fungsi lain dengan nama delFormat yang berfungsi untuk menghapus sub string .png dari nama file.
``` 
void delFormat(char str[], char sub_str[]){
    size_t len = strlen(sub_str);
    char *s = str;
    while ((s = strstr(s, sub_str)) != NULL){
        memmove(s, s + len, strlen(s + len) + 1);
    }
}
```

### 2. Mengubah Delimiter “_” menjadi “;”
Karena beberapa file poster terdiri dari dua poster yang dipisahkan dengan karakter “_” digunakanlah perintah strstr untuk mengecek apakah string mengandung karakter tersebut.
```
if (strstr(delete, "_")){
  for (int i=0; i<length; i++){
    if (delete[i] == '_'){
      delete[i] = ';';
      break;
    }
  }
}
```

### 3. Memisahkan Informasi pada Poster(Judul, Tahun, dan Genre)
Untuk menguraikan informasi tersebut digunakan strtok dengan mencari delimiter “;” yang memisahkan masing-masing informasi. Dengan menggunakan bantuan variabel index yang sudah diset 1, program akan melakukan 3 jenis pengecekan berdasarkan jumlah index pada saat itu. Saat index dimodulo 3 berjumlah 1 maka proses sedang berada di substring sebelum delimiter pertama yang berisi informasi judul film, jika jumlahnya 2 maka proses sedang berada pada substring setelah delimiter pertama sebelum delimiter kedua yang berisikan informasi tahun rilis film, dan yang terakhir apabila hasil modulonya 0  maka proses berada pada substring setelah delimiter kedua yang isinya adalah informasi genre film. Pada proses ini program akan membuat folder sesuai genre secara otomatis. Program akan mengecek variabel dest yang berisi path genre film apabila belum ada folder genre akan memanggil fungsi makeFolder. Proses dilanjutkan untuk melakukan penamaan ulang sesuai format dengan melakukan swaping pada 3 variabel berbeda yaitu src, dest, dan from yang akan menyimpan path yang benar dan akan memanggil fungsi renameFile. Selanjutnya informasi judul dan tahun rilis film yang sudah terpisah disimpan pada sebuah variabel global bernama release dan dipisahkan delimiter "_" yang akan membantu dalam proses sorting dan input ke file data.txt.
```
void clasify(char path[]){
    DIR *dp;
    struct dirent *dir;

    dp = opendir(path);

    if(dp){
        while ((dir = readdir(dp)) != NULL){
            if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..")){
                char delete[100];
                strcpy(delete, dir->d_name);
                //menghapus format .png
                delFormat(delete, ".png");

                //mengubah delimeter "_" menjadi ";"
                int length = strlen(delete);
                if (strstr(delete, "_")){
                    for (int i=0; i<length; i++){
                        if (delete[i] == '_'){
                            delete[i] = ';';
                            break;
                        }
                    }
                }

                char title[50], year[5], genre[20];
                int index = 1;
                char *tok = strtok(delete, ";");

                //menguraikan elemen file (judul, tahun, dan genre)
                while (tok != NULL){
                    char src[100], dest[100], from[100];

                    strcpy(src, path);
                    strcpy(dest, path);
                    strcpy(from, path);
                    //membuat folder sesuai genre
                    if (index % 3 == 0){
                        strcpy(genre, tok);
                        strcat(src, dir->d_name);
                        strcat(dest, genre);

                        DIR *myfolder = opendir(dest);
                        if (myfolder){
                            closedir(myfolder);
                        }
                        else if (ENOENT == errno){
                            makeFolder(dest);
                            strcpy(kategori[jenis++], genre);
                        }

                        copyFile(src, dest);
                        strcat(from, genre);
                        strcat(from, "/");
                        strcat(from, dir->d_name);

                        strcat(dest, "/");
                        strcat(dest, title);
                        strcat(dest, ".png");

                        renameFile(from, dest);
                        //menyimpan informasi tahun dan judul pada varibael release
                        for (int i=0; i<jenis; i++){
                            if (strcmp(kategori[i], genre) == 0){
                                char tmp[50];
                                strcpy(tmp, year);
                                strcat(tmp, "_");
                                strcat(tmp, title); 
                                strcpy(release[i][last[i]++],tmp);
                            }
                        }
                    }
                    //menguraikan judul
                    else if (index % 3 == 1){
                        strcpy(title, tok);
                    }
                    //menguraikan tahun
                    else if (index % 3 == 2){
                        strcpy(year, tok);
                    }
                    tok = strtok(NULL, ";");
                    index++;
                }
            }
        }
        closedir(dp);
    }  
}
```

Fungsi renameFile menggunkan command "mv" untuk memindahkan path yang sudah sesuai ke tempatnya.
```
void renameFile(char src[], char path[]){
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0){
        char *argv[] = {"mv", src, path, NULL};
        execv("/bin/mv", argv);
    }

    while ((wait(&status)) > 0);
}
```

## C. Sorting Tahun Rilis Film Secara Ascending
Pada tahap ini program akan memanggil fungsi sort yang akan melakukan looping berkali-kali untuk mengakses informasi judul dan tahun rilis pada variabel release dan dilakukan swaping dengan bantuan variabel tmp untuk mengurutkan tahun rilis film secara ascending.
```
void sort(){
    char tmp[100];

    for(int i=0; i<jenis; i++){
        for(int j=0; j<last[i]; j++){
            for(int k=0; k<last[i]-j-1; k++){
                if(strcmp(release[i][k], release[i][k+1]) > 0){
                    //swaping tahun rilis
                    strcpy(tmp, release[i][k]);
                    strcpy(release[i][k], release[i][k+1]);
                    strcpy(release[i][k+1], tmp);
                }
            }
        }
    }
}
```

## D. Membuat file data.txt
Tahapan selanjutnya yaitu membuat file data.txt yang berisi informasi nama dan tahun rilis film sesuai genre dengan format:
kategori : romance

nama : start-up
rilis : tahun 2020

nama : twenty-one-twenty-five
rilis : tahun 2022

Untuk melakukan hal tersebut, program akan memanggil fungsi printMovie yang melakukan nested looping. Pada looping parentnya sebanyak jenis genre film dimana pada setiap looping terdapat variabel dest yang akan diisikan path folder drakor dan genre pada index ke i. Looping selanjutnya sebanyak index kedua variabel release yaitu banyak film pada satu genre dengan urutan prosesnya yaitu yang pertama akan memisahkan judul dan tahun rilis film menggunakan strtok untuk mencari delimiter "_" yang sebelumnya ditambah pada fungsi clasify dengan bantuan varibel index yang dimodulo 2. Proses selanjutnya yaitu memasukan informasi-informasi yang dibutuhkan pada file data.txt. Pada tahap ini variabel char loc yang isinya disalin dari variabel dest sebelumnya yang berisi path ke folder masing-masing genre akan ditambahkan karakter "/data.txt" menggunakan strcat. Selanjutnya untuk mengecek keberadaan file data.txt pada path masing-masing genre digunakan fungsi acces() pada variabel loc dengan amode F_OK. Apabila file belum terbuat maka program akan otomatis membuat filenya dan menambahkan format yang dinginkan yaitu kategori, judul, dan tahun rilis film. Sedangkan jika filenya sudah terbuat yang berarti looping sudah melewati looping pada index 0 maka program hanya akan menambahkan judul dan tahun rilis dari film dengan genre yang sama.
```
void printMovie(char path[]){

    for (int i=0; i<jenis; i++){
        char dest[100];

        strcpy(dest, path);
        strcat(dest, kategori[i]);
        for(int j=0; j<last[i]; j++){
            char edit[100];

            strcpy(edit, release[i][j]);

            char title[50], year[5];
            int index = 1;
            char *tok = strtok(edit, "_");
            //memisahkan tahun rilis dan judul pada variabel release
            while (tok != NULL){
                if (index % 2 == 0){
                    strcpy(title, tok);
                }
                else if (index % 2 == 1){
                    strcpy(year, tok);
                }
                tok = strtok(NULL, ";");
                index++;
            }

            //memasukkan informasi film ke file data.txt
            char loc[150];
            strcpy(loc, dest);
            strcat(loc, "/data.txt");
            if (access(loc, F_OK    ) == 0){
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "\nnama : %s\nrilis : tahun %s\n", title, year);
                fclose(wfile);
            }else{
                FILE *wfile = fopen(loc, "w");
                fprintf(wfile, "kategori : %s\n\nnama : %s\nrilis : tahun %s\n", kategori[i], title, year);
                fclose(wfile);
            }
        }
    }
}
```
## Output

#### 1. Kompilasi File soal2.C
![satu](img/2.1.png)

#### 2. Run File soal2.C
![dua](img/2.2.png)

#### 3. Setelah dirun Program akan Menghasilkan 2 Folder Baru yaitu shift2 dan drakor didalamnya
![tiga](img/2.3.png)

#### 4. Berikut adalah isi dari Folder drakor yang terdiri dari beberapa folder genre dan file poster film yang telah diekstrak
![empat](img/2.4.png)

#### 5. Contoh isi folder genre pada folder romance yang berisikan sebuah file data.txt dan file poster yang telah direname
![lima](img/2.5.png)

#### 6. Contoh isi dari file data.txt pada folder romance
![enam](img/2.6.png)

## Kendala
- Membuat fungsi clasify untuk mengkategorikan film berdasarkan genrenya dengan cara mengelola variabel yang berisi directory dan keterkaitannya satu sama lain.

# Soal 3
## Catatan
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
-	Gunakan exec dan fork
-	Direktori “.” dan “..” tidak termasuk

## A. Membuat Folder modul2
Untuk dapat membuat folder dengan menggunakan bahasa c digunakanlah execlp untuk menjalankan argumen sebagai command lalu digunakan -p agar jika parent folder belom dibuat maka parent folder akan dibuat juga. Lalu digunakan "while(wait(&status)) > 0" agar tidak exit dari function sebelum tugas dalam function selesai dikerjakan.
```
 void createDirectory(char path[])
{
  if (fork() == 0)
    execlp("mkdir", "mkdir", "-p", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

### 1. Membuat Folder darat
Digunakanlah function createDirectory yang telah dijelaskan diatas dengan memasukkan path untuk folder darat pada main.
```
char pathDarat[] = "/home/cahyadi/modul2/darat/";
createDirectory(pathDarat);
```
### 2. Setelah 3s Membuat Folder air
Digunakanlah function createDirectory yang telah dijelaskan diatas dengan memasukkan path untuk folder air pada main.
```
char pathAir[] = "/home/cahyadi/modul2/air/";
sleep(3);
createDirectory(pathAir);
```

## B. Melakukan Unzip File animal.zip
Sama untuk menjalankan command unzip pada script c digunakanlah execlp diisi dengan command yang ingin dijalankan yaitu unzip dan digunakan -d untuk membuat destinasi untuk hasil dari unzip dan digunakan -q ("quiet") agar tidak menghasilkan verbose pada terminal. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan. 
```
void unzip(char destination[], char target[])
{
  if (fork() == 0)
    execlp("unzip", "unzip", "-d", destination, "-q", target, NULL);

  while ((wait(&status)) > 0)
    ;
}
```

Digunakanlah function unzip yang telah dijelaskan diatas dengan mengisi path untuk target dan destination pada main.
```
char path[] = "/home/cahyadi/modul2/";
char targetZIP[] = "animal.zip";
unzip(path, targetZIP);
```

## C. Memindahkan file dari animal.zip
Pertama-tama kita akan membuka folder yang diinginkan yaitu folder animal lalu melakukan iterasi selama tidak NULL atau selama cursor file masih ada. Lalu setiap iterasi file pada folder dilakukan pengcopyan nama file (dir->d_name) ke array global hewan serta menambahkan sebanyak satu pada variabel count untuk menghitung berapa banyak jumlah file yang ada pada folder yang dibuka. Lalu jika sudah selesai melakukan iterasi pada folder, folder akan ditutup. Lalu dilakukanlah penglistan hewan sesuai kategori yang ada yaitu "air", "darat", dan "burung" dengan melakukan iterasi sebanyak jumlah hewan yang ada. Jika nama hewan mengandung kata "burung" (dapat digunakan fungsi string strstr) maka nama hewan tersebut akan dicopy ke array burung dan variabel burungSize akan ditambah sebanyak 1 (banyaknya jumlah file yang mengandung kata burung). Lalu jika hewan juga mengandung kata "darat" maka nama hewan tersebut juga akan dicopy ke array hewanDarat dan variabel countDarat akan ditambah sebanyak 1 (banyaknya jumlah file mengandung kata darat) dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "darat". Namun jika nama hewan mengandung kata "air" maka nama hewan tersebut akan dicopy ke array hewanAir dan variabel countAir akan ditambah sebanyak 1 dan akan melakukan pemanggilan function moveAnimalDaratOrAir dengan parameter type adalah "air".
```
void listAnimal(char path[])
{
  DIR *d;
  struct dirent *dir;
  int count = 0, countDarat = 0;
  d = opendir(path);

  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      if ((strstr(dir->d_name, "..") == 0) && (strcmp(dir->d_name, "") != 0) && (strcmp(dir->d_name, ".") != 0) && (strstr(dir->d_name, "txt") == 0))
      {
        strcpy(hewan[count], dir->d_name);
        count++;
      }
    }
    closedir(d);
  }

  for (int i = 0; i < count; i++)
  {
    char copyPath[100];
    strcpy(copyPath, path);

    if (strstr(hewan[i], "bird"))
    {
      strcpy(burung[burungSize], hewan[i]);
      burungSize++;
    }
    if (strstr(hewan[i], "darat"))
    {
      strcpy(hewanDarat[countDarat], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "darat");
      countDarat++;
    }
    else if (strstr(hewan[i], "air"))
    {
      strcpy(hewanAir[countAir], hewan[i]);
      moveAnimalDaratOrAir(copyPath, i, "air");
      countAir++;
    }
  }
}
```

Untuk memindahkan dapat digunakan dengan "execlp("mv", "mv", path, dest, NULL)" dimana path adalah path source dari file dan dest adalah path source setelah dipindahkan nantinya. Untuk menentukan pembagiannya sesuai kategori digunakanlah strcmp antara type dan "air" dimana jika menghasilkan 0 maka artinya type dan "air" sama dan merupakan kategori air maka melakukan pengcopyan path air ke variabel dest, begitupun sebaliknya. Digunakkan while(wait(&status)) untuk dapat menghindari exit function sebelum script selesai dijalankan. 
```
void moveAnimalDaratOrAir(char path[], int index, char *type)
{
  strcat(path, hewan[index]);
  char dest[100];
  if (strcmp(type, "air") == 0)
    strcpy(dest, "/home/cahyadi/modul2/air");
  else
    strcpy(dest, "/home/cahyadi/modul2/darat");
  if (fork() == 0)
  {
    execlp("mv", "mv", path, dest, NULL);
  }
  while ((wait(&status)) > 0)
    ;
}
```

Dipanggilah function listAnimal dengan path animal untuk memindahkan file ke folder kategorinya masing-masing dan melakukan penglistan nama file hewan ke global array yang telah disediakan sesuai dengan kategori air dan darat pada fungsi main.
```
char pathAnimal[] = "/home/cahyadi/modul2/animal/";
listAnimal(pathAnimal);
```

### 1. Menghapus file yang tidak mengandung baik "air" maupun "darat"
Untuk dapat menghapus file serta folder yang tidak dibutuhkan yaitu animal dan file yang tidak sesuai dengan format dapat dilakukan langsung dengan rm -r ("recursive") path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
void removeDirectory(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "-r", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Karena kita ingin menghapus file yang berada di dalam folder animal serta folder animal, maka kita bisa langsung saja menghapus satu direktori dengan menggunakan function removeDirectory dan memparse path untuk animal pada main. 
```
char pathAnimal[] = "/home/cahyadi/modul2/animal/";
removeDirectory(pathAnimal);
```

## D. Menghapus file yang mengandung "burung" dari folder darat
Untuk dapat menghapus file yang diinginkan dapat dilakukan langsung dengan rm --force path. Lalu digunakan while wait untuk dapat menghindari exit function sebelum script selesai dijalankan.
```
void removeFile(char path[])
{
  int status;
  if (fork() == 0)
    execlp("rm", "rm", "--force", path, NULL);
  while ((wait(&status)) > 0)
    ;
}
```

Melakukan concanate string path darat dengan nama file burung lalu melakukan penginterasian sebanyak jumlah file yang mengandung kata "burung" pada file darat.
```
void removeBirdFile()
{
  for (int i = 0; i < burungSize; i++)
  {
    char source[100] = "/home/cahyadi/modul2/darat/";
    strcat(source, burung[i]);
    removeFile(source);
  }
}
```

Memanggil function removeBirdFile yang telah dijelaskan diatas pada main
```
removeBirdFile();
```

## E. Melakukan list untuk nama file dari folder air ke list.txt
Untuk format nama list yang digunakkan adalah UID_UID Permission(rwx)_namafile.jpg. Untuk mendapat UID dapat dengan mudah bisa didapatkan dengan melakukan check stat pada folder air lalu digunakanlah getpwuid dari path yang diisi, lalu untuk mendapat nama dapat dengan mudah dilakukan pw->pw_name. Lalu untuk mendapat UID Permission didapatkan dengan membuat char array dan dicek satu persatu untuk read, write, dan execute dengan S_IRUSR(untuk r), S_IRUSR(untuk w), dan S_IRUSR(untuk x). Lalu untuk tahap terakhir dapat digunakan dengan I/O pada C. Digunakkan open file dengan mode "a" (append) dan untuk menulis per line digunakan fprintf dengan format yang telah diminta. Lalu setelah selesai menggunakan file, akan dilakukan penutupan file untuk mencegah memory leak
```
void addList(int index)
{
  char permission[5];
  struct stat fs;
  int r;
  char path[] = "/home/cahyadi/modul2/air/";
  r = stat(path, &fs);
  if (r == -1)
  {
    fprintf(stderr, "File error\n");
    exit(1);
  }

  struct passwd *pw = getpwuid(fs.st_uid);
  if (fs.st_mode & S_IRUSR)
    permission[0] = 'r';
  if (fs.st_mode & S_IRUSR)
    permission[1] = 'w';
  if (fs.st_mode & S_IRUSR)
    permission[2] = 'x';

  if (pw != 0)
  {
    FILE *wfile = fopen("/home/cahyadi/modul2/air/list.txt", "a");
    fprintf(wfile, "%s_%s_%s\n", pw->pw_name, permission, hewanAir[index]);
    fclose(wfile);
  }
}
```

Melakukan pemanggilan function AddList yang telah dijelaskan diatas sebanyak file yang berada dalam folder air pada main.
```
for (int i = 0; i < countAir; i++)
    addList(i);
```

## Output

#### 1. Proses kompilasi file soal3.c
![satu](img/3.1.PNG)

#### 2. Menjalankan program file soal3.exe sehingga menghasilkan folder baru yaitu modul2
![dua](img/3.2.PNG)

#### 3. Hasil dari penjalanan program sebelumnya juga menghasilkan dua folder baru lainnya yaitu darat dan air yang memiliki parent folder modul2
![tiga](img/3.3.PNG)

#### 4. Perbedaan waktu pembuatan antara folder darat dan air yaitu 3s sesuai dengan permintaan soal
![empat](img/3.4.PNG)

#### 5. Isi dari folder darat yang merupakan gambar sesuai dengan ketentuan dari soal yang diminta
![lima](img/3.6.PNG)

#### 6. Isi dari folder air yang merupakan gambar sesuai dengan ketentuan dari soal yang diminta serta sebuah list.txt
![enam](img/3.5.PNG)

#### 7. Isi dari list.txt sesuai dengan yang diminta dari soal yaitu list nama file dalam folder air sesuai dengan format nama yang telah diberikan
![tujuh](img/3.7.PNG)

## Kendala
- Membuat fungsi yang tepat baik dari parameter dan logicnya sehingga tidak membuat code terlalu panjang dan lebih mudah dibaca

