#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

char release[10][10][100], kategori[10][50];
int jenis = 0, last[10], status;

void makeFolder(char path[]);
void unzip(char destination[], char target[]);
void delFormat(char str[], char sub_str[]);
void copyFile(char src[], char path[]);
void renameFile(char src[], char path[]);
void clasify(char path[]);
void sort();
void printMovie(char path[]);

int main(){
    char path[] = "/home/aga/shift2/drakor/";

    memset(last, 0, sizeof(last));
    makeFolder(path);
    unzip(path, "drakor.zip");
    clasify(path);
    sort();
    printMovie(path);

    return 0;
}

void makeFolder(char path[]){
    if (fork() == 0){
        execlp("mkdir", "mkdir", "-p", path, NULL);
    }

    while ((wait(&status)) > 0);
}

void unzip(char destination[], char target[]){
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0){

        char *argv[] = {"unzip", "-q", target, "*.png", "-d", destination, NULL};
        execv("/bin/unzip", argv);
    }

    while ((wait(&status)) > 0);
}

void delFormat(char str[], char sub_str[]){
    size_t len = strlen(sub_str);
    char *s = str;
    while ((s = strstr(s, sub_str)) != NULL){
        memmove(s, s + len, strlen(s + len) + 1);
    }
}

void copyFile(char src[], char path[]){
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0){
        char *argv[] = {"cp", src, path, NULL};
        execv("/bin/cp", argv);
    }

    while ((wait(&status)) > 0);
}

void renameFile(char src[], char path[]){
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0){
        char *argv[] = {"mv", src, path, NULL};
        execv("/bin/mv", argv);
    }

    while ((wait(&status)) > 0);
}

void clasify(char path[]){
    DIR *dp;
    struct dirent *dir;

    dp = opendir(path);

    if(dp){
        while ((dir = readdir(dp)) != NULL){
            if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..")){
                char delete[100];
                strcpy(delete, dir->d_name);
                //menghapus format .png
                delFormat(delete, ".png");

                //mengubah delimeter "_" menjadi ";"
                int length = strlen(delete);
                if (strstr(delete, "_")){
                    for (int i=0; i<length; i++){
                        if (delete[i] == '_'){
                            delete[i] = ';';
                            break;
                        }
                    }
                }

                char title[50], year[5], genre[20];
                int index = 1;
                char *tok = strtok(delete, ";");

                //menguraikan elemen file (judul, tahun, dan genre)
                while (tok != NULL){
                    char src[100], dest[100], from[100];

                    strcpy(src, path);
                    strcpy(dest, path);
                    strcpy(from, path);
                    //membuat folder sesuai genre
                    if (index % 3 == 0){
                        strcpy(genre, tok);
                        strcat(src, dir->d_name);
                        strcat(dest, genre);

                        DIR *myfolder = opendir(dest);
                        if (myfolder){
                            closedir(myfolder);
                        }
                        else if (ENOENT == errno){
                            makeFolder(dest);
                            strcpy(kategori[jenis++], genre);
                        }

                        copyFile(src, dest);
                        strcat(from, genre);
                        strcat(from, "/");
                        strcat(from, dir->d_name);

                        strcat(dest, "/");
                        strcat(dest, title);
                        strcat(dest, ".png");

                        renameFile(from, dest);
                        for (int i=0; i<jenis; i++){
                            if (strcmp(kategori[i], genre) == 0){
                                char tmp[50];
                                strcpy(tmp, year);
                                strcat(tmp, "_");
                                strcat(tmp, title); 
                                strcpy(release[i][last[i]++],tmp);
                            }
                        }
                    }
                    //menguraikan judul
                    else if (index % 3 == 1){
                        strcpy(title, tok);
                    }
                    //menguraikan tahun
                    else if (index % 3 == 2){
                        strcpy(year, tok);
                    }
                    tok = strtok(NULL, ";");
                    index++;
                }
            }
        }
        closedir(dp);
    }  
}

void sort(){
    char tmp[100];

    for(int i=0; i<jenis; i++){
        for(int j=0; j<last[i]; j++){
            for(int k=0; k<last[i]-j-1; k++){
                if(strcmp(release[i][k], release[i][k+1]) > 0){
                    //swaping tahun rilis
                    strcpy(tmp, release[i][k]);
                    strcpy(release[i][k], release[i][k+1]);
                    strcpy(release[i][k+1], tmp);
                }
            }
        }
    }
}

void printMovie(char path[]){

    for (int i=0; i<jenis; i++){
        char dest[100];

        strcpy(dest, path);
        strcat(dest, kategori[i]);
        for(int j=0; j<last[i]; j++){
            char edit[100];

            strcpy(edit, release[i][j]);

            char title[50], year[5];
            int index = 1;
            char *tok = strtok(edit, "_");
            //memisahkan tahun rilis dan judul pada variabel release
            while (tok != NULL){
                if (index % 2 == 0){
                    strcpy(title, tok);
                }
                else if (index % 2 == 1){
                    strcpy(year, tok);
                }
                tok = strtok(NULL, ";");
                index++;
            }

            //memasukkan informasi film ke file data.txt
            char loc[150];
            strcpy(loc, dest);
            strcat(loc, "/data.txt");
            if (access(loc, F_OK    ) == 0){
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "\nnama : %s\nrilis : tahun %s\n", title, year);
                fclose(wfile);
            }else{
                FILE *wfile = fopen(loc, "w");
                fprintf(wfile, "kategori : %s\n\nnama : %s\nrilis : tahun %s\n", kategori[i], title, year);
                fclose(wfile);
            }
        }
    }
}
